// imports
const express = require("express");
const fs = require("fs");
const morgan = require("morgan");
const router = require("./routes/router");
const app = express();

const PORT = 3000;

// middleware
app.use(express.json());
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));

//set views
app.set("view engine", "ejs");

//static file
app.use(express.static("public"));
app.use("/css", express.static(__dirname + "/public/assets/css"));
app.use("/js", express.static(__dirname + "/public/js"));
app.use("/img", express.static(__dirname + "/public/assets/img"));

//endpoint
app.use("/", router);

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
