const { render } = require("ejs");

module.exports = {
  index: (req, res, next) => {
    res.render("index");
  },
  rps: (req, res, next) => {
    res.render("rps");
  },
};
