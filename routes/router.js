const express = require("express");
const homeController = require("../controller/homeController");

const router = express.Router();

//middleware
router.use(function timeLog(req, res, next) {
  console.log("Time: ", Date.now());
  next();
});

//endpoint
router.get("/", homeController.index);
router.get("/rps", homeController.rps);

//error handler
router.use((req, res, next) => {
  const err = new Error("Page not found");
  err.status = 404;
  next(err);
});
router.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message,
    },
  });
});

module.exports = router;
